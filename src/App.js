import React from 'react';

import './App.css';
import Header from './Header';
import Question from './Question';
import Signup from './Signup';
import Toast from './Toast';

class App extends React.Component {
	ws = new WebSocket(process.env.REACT_APP_BACKEND_SOCKET);

	constructor(props) {
		super(props);

		this.state = {
			question: 0,
			name: localStorage.getItem('name'),
			toast: {
				message: '',
				type: 'error',
			},
		};
	}

	toast = (message, type = 'success') => {
		this.setState({ ...this.state, toast: { message, type } });

		setTimeout(() => this.setState({ ...this.state, toast: { message: '' } }), 3000);
	};

	componentDidMount() {
		this.ws.onopen = () => {
			// on connecting, do nothing but log it to the console
			console.log('connected');
		};

		this.ws.onmessage = (evt) => {
			// listen to data sent from the websocket server
			const message = evt.data;
			const reader = new FileReader();

			reader.addEventListener('loadend', () => {
				if (reader.result.substr(0, 17) === 'consumer.question') {
					const question = reader.result.split('.').pop();
					this.setState({ question });
				}
			});

			reader.readAsText(message);
		};

		this.ws.onclose = () => {
			console.log('disconnected');
			// automatically try to reconnect on connection loss
		};

		fetch(`${process.env.REACT_APP_BACKEND_HOST}/current`)
			.then((response) => response.json())
			.then((response) => this.setState({ ...this.state, question: response.question }));
	}

	render() {
		return (
			<div className="App">
				<Toast message={this.state.toast.message} type={this.state.toast.type} />
				<Header state={this.state} />
				{this.state.name && <Question toast={this.toast} question={this.state.question}></Question>}
				{!this.state.name && <Signup />}
			</div>
		);
	}
}

export default App;
