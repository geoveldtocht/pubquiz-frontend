import React, { useState } from 'react';
import Button from './Button';
import Input from './Input';

const Signup = () => {
	const [name, setName] = useState('');

	function submit(event) {
		event.preventDefault();

		localStorage.setItem('name', name);

		window && window.location && window.location.reload();
	}

	return (
		<form onSubmit={submit}>
			<Input name="name" onChange={(event) => setName(event.target.value)} value={name} />
			<br />
			<br />
			<br />
			<Button type="submit" disabled={name === ''}>
				start quiz
			</Button>
		</form>
	);
};

export default Signup;
