import React from 'react';
import styled from 'styled-components';

const Toast = ({ message, type }) => (
	<Toasty className={type} message={message}>
		{message}
	</Toasty>
);

const Toasty = styled.div`
	position: absolute;
	top: -100px;
	background: #65b3cd;
	width: 300px;
	font-size: 1.3rem;
	border-radius: ;
	padding: 16px 0;
	border-radius: 4px;
	color: #fff;
	transform: translateY(${(props) => (props.message === '' ? '0px' : '110px')});
	transition: transform 0.3s ease;
`;

export default Toast;
