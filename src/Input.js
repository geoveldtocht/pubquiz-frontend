import styled from 'styled-components';

const Input = styled.input`
	padding: 12px 14px;
	border: 0;
	outline: 0;
	border-radius: 8px;
`;

export default Input;
