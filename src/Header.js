import React from 'react';
import styled from 'styled-components';

const Header = ({ state }) => <StyledHeader>{state.name}</StyledHeader>;

const StyledHeader = styled.header`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 40px;
	text-align: right;
	padding: 10px 20px;
	font-size: 1rem;
`;

export default Header;
