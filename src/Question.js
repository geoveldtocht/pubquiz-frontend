import React, { useState } from 'react';
import styled from 'styled-components';
import connect from 'react-watcher';

import Button from './Button';

class Question extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			answer: '',
			submitted: false,
		};
	}

	submit(question, answer, toast) {
		this.setState({ ...this.state, submitted: true });

		fetch(`${process.env.REACT_APP_BACKEND_HOST}/submit`, {
			method: 'post',
			body: JSON.stringify({ question, name: localStorage.getItem('name'), answer }),
		})
			.then(() => toast('antwoord verstuurd 😁'))
			.catch(() => toast('er is iets misgegaan 😢'), 'error');
	}

	render() {
		const { watch } = this.props;
		watch('question', () => this.setState({ answer: '', submitted: false }));

		return (
			<div>
				<QuestionTitle>Vraag {this.props.question}</QuestionTitle>

				<Asnwers>
					{['a', 'b', 'c', 'd'].map((letter, index) => (
						<AnswerButton
							key={index}
							className={`letter-${letter} ${this.state.answer === letter ? 'active' : ''}`}
							onClick={() => this.setState({ ...this.state, answer: letter })}
						>
							{letter.toUpperCase()}
						</AnswerButton>
					))}
				</Asnwers>

				<CustomButton
					disabled={this.state.answer === '' || this.state.submitted}
					onClick={() => this.submit(this.props.question, this.state.answer, this.props.toast)}
				>
					versturen
				</CustomButton>
			</div>
		);
	}
}

export default connect(Question);

const QuestionTitle = styled.p`
	font-size: 3rem;
	margin-top: 0;
`;

const Asnwers = styled.div`
	display: flex;
	width: 340px;
	max-width: 100vw;
	padding: 0 20px;
	justify-content: space-between;
	flex-wrap: wrap;
`;

const AnswerButton = styled.a`
	display: inline-block;
	width: 150px;
	max-width: 40%;
	height: 100px;
	text-align: center;
	line-height: 100px;
	background: #fff;
	border-radius: 8px;
	margin: 20px 0;
	font-size: 2rem;
	color: #073b4c;
	user-select: none;
	cursor: pointer;
	transition: all 0.2s ease;

	&.active {
		box-shadow: 0 12px 32px 0 rgba(0, 0, 0, 0.4);
		transform: scale(1.1);
		color: #fff;
	}

	&.letter-a.active {
		background: #ef476f;
	}

	&.letter-b.active {
		background: #ffd166;
	}

	&.letter-c.active {
		background: #06d6a0;
	}

	&.letter-d.active {
		background: #118ab2;
	}
`;

const CustomButton = styled(Button)`
	margin-top: 40px;
`;
