import styled from 'styled-components';

const Button = styled.button`
	border: 2px solid #65b3cd;
	background: #65b3cd;
	color: #fff;
	width: 100px;
	height: 40px;
	border-radius: 20px;
	outline: none;
	cursor: pointer;

	&[disabled] {
		opacity: 0.4;
	}
`;

export default Button;
